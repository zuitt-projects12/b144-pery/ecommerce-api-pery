const jwt = require("jsonwebtoken");
const secret = "FI-NO-IS-CH-SE-DK";

module.exports.createAccessToken = (user) => {
	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}; // end const data

	return jwt.sign(data, secret, {});
}; // end module.exports.createAccessToken

// Token verification
module.exports.verify = (req, res, next) => {
	let token = req.headers.authorization;
	if(typeof token !== "undefined"){
		token = token.slice(7, token.length);
		return jwt.verify(token, secret, (err, data) => {
			if(err){
				return res.send({auth: "failed"});
			}
			else{
				next();
			}
		}); // end return jwt.verify()
	} // end if(typeof token !== "undefined")
	else{
		return res.send({auth: "failed"});
	} // end else if(typeof token !== undefined)
}; // end module.exports.verify

// Token decryption
module.exports.decode = (token) => {
	if(typeof token !== "undefined"){
		token = token.slice(7, token.length);
		return jwt.verify(token, secret, (err, data) => {
			if(err){
				return null;
			}
			else{
				return jwt.decode(token, {complete: true}).payload;
			} // end else if(err)
		}); // end return jwt.verify()
	} // end if(typeof token !== "undefined")
	else{
		return null;
	} // end else if(typeof token !== "undefined")
}; // end module.exports.decode