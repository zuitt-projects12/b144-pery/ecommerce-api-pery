// REQUIRED MODULES
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
// Routes modules
const userRoutes = require("./routes/user");
const productRoutes = require("./routes/product");
const orderRoutes = require("./routes/order");

// Server setup
const app = express();
const port = 3001;
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}));
// Routes
app.use("/api/users", userRoutes);
app.use("/api/products", productRoutes);
app.use("/api/orders", orderRoutes);


// DB connection
mongoose.connect("mongodb+srv://admin:admin@wdc028-course-booking.dwfxo.mongodb.net/ecommerce-api?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

let db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error"));
db.once("open", () => console.log("We're connected to the MongoDB Atlas"));

app.listen(process.env.PORT || port, () => console.log(`Server running at port ${process.env.PORT || port}`));