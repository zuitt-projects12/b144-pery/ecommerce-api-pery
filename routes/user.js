const express = require("express");
const router = express.Router();
const auth = require("../auth");
const userController = require("../controllers/user");
const Product = require("../models/Product");

// Route for checking if email exists
router.post("/checkEmail", (req, res) => {
	userController.checkEmailExists(req.body).then(result => res.send(result));
});

// Route for signing up
router.post("/signup", (req, res) => {
	userController.signupUser(req.body).then(result => res.send(result));
});

// Route for logging in.
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(result => res.send(result));
});

// Routes for retrieving a user by ID.
router.get("/details", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	userController.getProfile(userData).then(result => res.send(result));
});

// Route for setting user as admin.
router.patch("/:userId/setasadmin", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	const data = {
		changeToAdminId: req.params.userId, // The user ID that has to be set to admin.
		sessionUserId: userData.id // The user ID that is logged in that is retrieved thru JWT.
	};
	userController.setAsAdmin(data).then(result => res.send(result));
});

// Route for changing the password
router.patch("/changepassword", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	userController.changePassword(userData, req.body).then(result => res.send(result));
});

// Route for viewing cart items
router.get("/cart", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	userController.viewCartItems(userData).then(result => res.send(result));
});
// Route for increment cart item
router.get("/cart/:productId/increment", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	userController.incrementCartItem(userData, req.params.productId).then(result => res.send(result));
})
;// Route for decrement cart item
router.get("/cart/:productId/decrement", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	userController.decrementCartItem(userData, req.params.productId).then(result => res.send(result));
});

// Route for removing an item in the cart
router.post("/cart/remove", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	userController.removeItemInCart(userData, req.body).then(result => res.send(result));
});

// Route for adding items to cart
router.post("/addtocart", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	userController.addToCart(userData, req.body).then(result => res.send(result));
});

module.exports = router;